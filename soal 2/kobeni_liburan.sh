#!/bin/bash

#input jam saat ini
time_now=$(date +"%H")

#jika pukul 00 download 1 kali, selain itu sesuai jam
if [ $time_now == 00 ]
then
	x=1
else
	x=$time_now
fi
echo "x adalah $x"

#membuat folder
currfolder=$(ls -d kumpulan_* | wc -l)
nextfolder=$((currfolder + 1))

#download file
for ((num=1; num<=x; num=num+1))
do
	curl https://www.panorama-destination.com/wp-content/uploads/2019/04/Mountain-Bromo-2.jpg --create-dirs -o $PWD/kumpulan_$nextfolder/perjalanan_$num.jpg
done

#cronjobs download setiap 10 jam
#0 */10 * * * $PWD/kobeni_liburan.sh

#zip.sh
#currzip=$(ls -d devil_* | wc -l)
#nextzip=$((currzip + 1))
#zip -r devil_$nextzip kumpulan_*
#rm -r kumpulan*

#cronjobs zip setiap 1 hari
#0 0 * * * $PWD/zip.sh
