#!/bin/bash

#Membuat file user.txt dan log.txt  jika belum ada, jika sudah ada akan diskip
if [ ! -f "user.txt" ]; then
	touch user.txt
fi

if [ ! -f "log.txt" ]; then 
	touch log.txt
fi

# Membuat variabel date yang berisi waktu dengan format DD
date=$(date '+%Y/%m/%d %H:%M:%S')

# Menampilkan tampilan awal untuk memasukkan username dan password
echo -e "\nWelcome to Register" #-e digunakan agar bisa memberikan endl sebelumnya
echo -e "\nEnter username: "
read username
echo -e "\nEnter password: "
read -s password #-s untuk tidak menampilkan preview password yang ingin dimasukkan


#Memeriksa apakah username dan password sudah ada
#dengan cara mengecek pada users.txt 
if grep -q -w "${username}" user.txt ; then
	echo "${date} REGISTER: ERROR User already exists"
	bash retep.sh #jika user sudah ada, akan dialihkan ke retep.sh untuk login
else 
	#Mengecek apakah password tidak sama dengan username
	if [[ ${username} == ${password} ]]; then
		echo "ERROR: Password must be different with username"
	#Mengecek apakah password sudah berisi minimal 1 huruf kapital dan 1 huruf kecil
	elif ! [[ ${password} =~ [A-Za-z] ]]; then 
		echo "ERROR: Password must contain at least 1 uppercase and 1 lowercase"
	#Mengecek apakah panjang password tidak kurang dari 8
	elif ! [[ ${password} -lt 8 ]]; then
		echo "ERROR: Password must than 8 characters"
	#Mengecek apakah password sudah mengandung numerik
	elif ! [[ ${password} =~ [0-9] ]]; then 
                echo "ERROR: Password must alphanumeric"
	#Mengecek apakah password tidak mengandung kata chicken dan ernie
	elif [[ ${password}  =~ "chicken" ]]; then
  		echo "ERROR: Password can not contain chicken"
	elif [[ ${password}  =~ "ernie" ]]; then
                echo "ERROR: Password can not contain ernie"

	else 
		echo -e "\n${date} REGISTRATION SUCCESS"
		#Waktu login dan message akan diprint dalam file log.txt
		echo -e "\n${date} REGISTER: INFO User ${username} registered successfully" >> log.txt
		#Data user akan disimpan dalam user.txt
		echo "$username $password" >> user.txt
	fi
fi


