#!/bin/bash

#mengambil rot cipher sesuai jam sekarang
rot=$(date +%H)

#backup syslog
cp /var/log/syslog $PWD

#mengganti alfabet sesuai soal
lower=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
upper=ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ

lower_encrypt=$(echo $lower | tr "${lower:0:26}" "${lower:${rot}:26}")
upper_encrypt=$(echo $upper | tr "${upper:0:26}" "${upper:${rot}:26}")

#encrypt backup syslog
tr "${lower}${upper}" "${lower_encrypt}${upper_encrypt}" < $PWD/syslog > $PWD/syslogenc.txt
mv syslogenc.txt "$(date +%H:%M\ %d:%m:%Y).txt"
rm syslog

#cronjob backup setiap 2 jam
#0 */2 * * * $PWD/log_encrypt.sh
