#!/bin/bash

#a
awk -F',' '/Japan/ {print $2}'  University_Rankings.csv | head -5

#b
echo -e '\nnumber2'
LC_ALL=C sort -t',' -k9n University_Rankings.csv | awk -F',' '/Japan/ {print $2}' | head -5

#c
echo -e '\nnumber3'
sort -t',' -k20 University_Rankings.csv | awk -F',' '/Japan/ {print $2}' | head -10

#d
echo -e '\nnumber4'
awk -F',' '/Keren/ {print $2}' University_Rankings.csv
